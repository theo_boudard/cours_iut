#include <stdio.h>
#include <stdlib.h>

void saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

void inversion(char* chaine1)
{
    int taille,i;
    char tmp;
    taille = strlen(chaine1)-1;
    i=0;
    while(i<taille-i)
    {
        tmp = chaine1[i];
        chaine1[i] = chaine1[taille-i];
        chaine1[taille-i] = tmp;
        i++;
    }
}

int main() {
    char chaine1[100];

    printf("Saisir une chaine de caractere :\n");
    saisie(chaine1);
    printf("\n-> %s\n",chaine1);

    inversion(chaine1);
    printf("\nChaine invers�e -> %s\n",chaine1);

    return 0;
}
