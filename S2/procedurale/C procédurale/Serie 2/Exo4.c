#include <stdio.h>
#include <stdlib.h>

int saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
    return(i);
}

int main() {
    char chaine[100];
    int compteur;
    printf("Saisir une chaine de caractere :\n");
    compteur = saisie(chaine);
    printf("\n-> %s\nLa chaine contient %d caracteres\n",chaine,compteur);

    return 0;
}
