#include <stdio.h>
#include <stdlib.h>

void saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

void fusion(char* chaine1, char* chaine2, char* chaine3)
{
    int taille,i;
    taille = strlen(chaine1);
    i=0;
    while(chaine1[i] != '\0')
    {
        chaine3[i] = chaine1[i];
        i++;
    }

    i=0;
    while(chaine2[i] != '\0')
    {
        chaine3[taille+i] = chaine2[i];
        i++;
    }
    chaine3[taille+i] = '\0';
}

int main() {
    char chaine1[100],chaine2[100],chaine3[100];

    printf("Saisir une chaine de caractere :\n");
    saisie(chaine1);
    printf("Saisir une deuxieme chaine de caractere :\n");
    saisie(chaine2);

    printf("\n-> %s\n",chaine1);
    printf("\n-> %s\n",chaine2);

    fusion(chaine1, chaine2, chaine3);
    printf("\nChaine fusion -> %s\n",chaine3);

    return 0;
}
