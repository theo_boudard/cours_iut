#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void saisie(char *tab)
{
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

void transforme_maj(char* chaine1, char* chaine2)
{
    int i=0,diff;
    while(chaine1[i] != '\0')
    {
        chaine2[i] = toupper(chaine1[i]);
        i++;
    }
    chaine2[i]='\0';
}

int main() {
    char chaine1[100],chaine2[100];
    printf("Saisir une chaine de caractere :\n");
    saisie(chaine1);
    printf("\n-> %s\n",chaine1);
    transforme_maj(chaine1,chaine2);
    printf("\n-> %s\n",chaine2);

    return 0;
}
