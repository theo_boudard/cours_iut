#include <stdio.h>
#include <stdlib.h>

void saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

char* copie_pointeur(char* chaine1)
{
    char *p,*deb;
    int i=0,nb;
    nb = strlen(chaine1) + 1;
    deb = p = (char*)malloc(nb * sizeof(char));
    while((*p++ = *chaine1++) != '\0');

    return(deb);
}

int main() {
    char chaine1[100], *chaine2;

    printf("Saisir une chaine de caractere :\n");
    saisie(chaine1);
    printf("\nchaine1 -> %s\n",chaine1);
    chaine2 = copie_pointeur(chaine1);
    printf("chaine2 -> %s\n",chaine2);

    return 0;
}
