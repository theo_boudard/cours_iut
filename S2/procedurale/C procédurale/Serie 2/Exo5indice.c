#include <stdio.h>
#include <stdlib.h>

void saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

void copie_indice(char* chaine1, char* chaine2)
{
    int i=0;
    while(chaine1[i] != '\0')
    {
        chaine2[i] = chaine1[i];
        i++;
    }
    chaine2[i] = '\0';
}

int main() {
    char chaine1[100], chaine2[100];

    printf("Saisir une chaine de caractere :\n");
    saisie(chaine1);
    printf("\nchaine1 -> %s\n",chaine1);
    copie_indice(chaine1,chaine2);
    printf("\nchaine2 -> %s\n",chaine2);

    return 0;
}
