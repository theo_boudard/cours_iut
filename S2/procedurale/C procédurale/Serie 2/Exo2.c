#include <stdio.h>
#include <stdlib.h>

void saisie(char *tab){
    int i=0;
    do
    {
        tab[i]=getchar();
    }
    while (tab[i++]!='\n');
    tab[--i]='\0';
}

int main() {
    char chaine[100];
    printf("Saisir une chaine de caractere :\n");
    saisie(chaine);
    printf("\n-> %s\n",chaine);

    return 0;
}
