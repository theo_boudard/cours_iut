#include <stdio.h>
#include <stdlib.h>

char* saisie(){
    char tab[100],*p;
    int taille;
    gets(tab);
    taille = strlen(tab);
    p = (char*)malloc((taille+1)*sizeof(char));
    strcpy(p,tab);
    return(p);
}

int main() {
    char* chaine;
    printf("Saisir une chaine de caractere :\n");
    chaine = saisie();
    printf("\n-> %s\n",chaine);

    return 0;
}
