#include <stdio.h>
#include <stdlib.h>

void permutation(int *x, int *y)
{
    int valeur_permut;
    valeur_permut = *x;
    *x = *y;
    *y = valeur_permut;

    return *x,*y;
}

int main()
{
    int X, Y;

    printf("Saisir deux entiers :\n");
    scanf("%d",&X);
    scanf("%d",&Y);

    printf("Avant permutation :\nX = %d\nY = %d\n", X, Y);

    permutation(&X,&Y);

    printf("Apres permutation\nX = %d\nY = %d", X, Y);

    return 0;
}
