#include <stdio.h>
#include <stdlib.h>

void factorielle(int n, int *fact)
{
    int cpt;
    *fact=1;

    for(cpt=1 ; cpt<=n ; cpt++)
    {
        *fact = *fact * cpt;
    }

    return *fact;
}



void puissance(int *x,int *y,int *puiss)
{
    int cpt;
    *puiss = 1;

    for(cpt=1 ; cpt<=*y ; cpt++)
    {
        *puiss = *puiss * *x;
    }

    return *puiss;
}



void trianglepascal(int *n,int *p,int *triangle_pascal)
{
    printf("J'ai pas fini le calcul du triangle de pascal");
}



int main()
{
    int N,fact=0,select,X,Y,puiss,n,p,triangle_pascal;

    printf("Saisir un calcul :\n1 : Factorielle\n2 : Puissance\n3 : Triangle de Pascal\n");
    scanf("%d", &select);

    switch(select)
    {
    case 1 :
        do
        {
            printf("Choisir un entier positif : ");
            scanf("%d", &N);
            if (N<0)
                printf("Erreur : entier n�gatif \n");
        }
        while(N<0);
        factorielle(N,&fact);
        printf("La factorielle de %d est : %d", N, fact);
        break;



    case 2 :
        do
        {
            printf("Choisir un entier positif : ");
            scanf("%d", &X);
            if (X<0)
                printf("Erreur : entier n�gatif \n");
        }
        while(X<0);

        do
        {
            printf("Choisir une puissance (enti�re) : ");
            scanf("%d", &Y);
            if (Y<0)
                printf("Erreur : entier n�gatif \n");
        }
        while(Y<0);

        puissance(&X,&Y,&puiss);
        printf("%d puissance %d = %d", X, Y, puiss);
        break;



    case 3 :
        do
        {
            printf("Choisir le param�tre n : ");
            scanf("%d", &n);
            if (n<0)
                printf("Erreur : entier n�gatif \n");
        }
        while(n<0);

        do
        {
            printf("Choisir le param�tre p : ");
            scanf("%d", &p);
            if (p<0)
                printf("Erreur : entier n�gatif \n");
        }
        while(p<0);
        trianglepascal(&n,&p,&triangle_pascal);
        break;
    }



    return 0;
}
