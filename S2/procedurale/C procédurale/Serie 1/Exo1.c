#include <stdio.h>
#include <stdlib.h>

void somme(int *limit,int *som)
{
    int cpt;
    *som=0;

    for (cpt=1 ; cpt <=*limit ; cpt++)
    {
        *som = *som + cpt;

    }
    return *som;
}

int main()
{
    int N,som=0;

    printf("Hello BOYS!\n");
    do
    {
        printf("Choisir un entier positif : ");
        scanf("%d", &N);
        if (N<=0)
            printf("Erreur, entier incorrect \n");
    }
    while(N<=0);
    somme(&N,&som);
    printf("La somme des %d premiers entiers positifs est : %d", N, som);

    return 0;
}
