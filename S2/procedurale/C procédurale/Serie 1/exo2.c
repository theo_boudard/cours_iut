#include <stdio.h>
#include <stdlib.h>

void moyenne(int *somme, int *compteur, float *moy)
{
    *moy = *somme / *compteur;

    return *moy;
}

int main()
{
    float moy;
    int note,cpt,som;

    som=0;
    cpt=0;
    moy=0;

    printf("Hello BOYS!\n");
    printf("Saisir 21 pour finir la saisie\n");
    printf("Saisir une note (0-20) : \n");
    do
    {
        scanf("%d", &note);
        if (note>21 || note<0)
            printf("Erreur, note incorrecte \n");
        else
        {
            if(note==21)
            {
                printf("Saisie termin�e\n");
            }
            else
            {
                som = som + note;
                cpt++;
            }
        }
    }
    while(note!=21);
    moyenne(&som, &cpt, &moy);
    printf("La moyenne des notes saisies est : %f", moy);

    return 0;
}
